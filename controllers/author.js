const Model = require('../models/author');
const fs = require('fs');

class Controller {

    constructor() {
        Model.find({}, (err, docs) => {
            if (!err && !docs.length) {
                fs.readFile('public/data/authors.json', (err, jsonData) => {
                    if (err) throw err;
                    let data = JSON.parse(jsonData);
                    let dataToInsert = [];
                    data.forEach(row => {
                        let [firstName, lastName] = row.split(' ');
                        dataToInsert.push({ firstName, lastName });
                    });
                    Model.insertMany(dataToInsert);
                });
            }
        });
    }

    getAll(req, res, next) {
        Model.find({}, (err, response) => {
            if (err) return next(err);
            res.status(200).send({ success: true, response });
        })
    }

    get(req, res, next) {
        let { id } = req.params;
        Model.findOne({ _id: id }, (err, response) => {
            if (err) return next(err);
            res.status(200).send({ success: true, response });
        });
    }

    post(req, res, next) {
        let body = req.body;
        let doc = new Model(body);
        doc.save((err, response) => {
            if (err) return next(err);
            res.status(200).send({ success: true, response });
        });
    }

    put(req, res, next) {
        let { id } = req.params;
        let body = req.body;
        Model.updateOne({ _id: id }, {
            $set: body
        }, (err, response) => {
            if (err) return next(err);
            res.status(200).send({ success: true, response });
        });
    }

    delete(req, res, next) {
        let { id } = req.params;
        Model.findByIdAndDelete({ _id: id }, (err, response) => {
            if (err) return next(err);
            res.status(200).send({ success: true, response });
        })
    }

}

const controller = new Controller();
module.exports = controller;