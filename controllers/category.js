const Model = require('../models/category');
const fs = require('fs');

class Controller {

    constructor() {
        Model.find({}, (err, docs) => {
            if (!err && !docs.length) {
                fs.readFile('public/data/categories.json', (err, jsonData) => {
                    if (err) throw err;
                    let data = JSON.parse(jsonData);
                    let dataToInsert = [];
                    data.forEach(row => {
                        dataToInsert.push({
                            name: row,
                            slug: row.toLowerCase().split(' ').join('-')
                        })
                    });
                    Model.insertMany(dataToInsert);
                });
            }
        });
    }

    /**
     * the result of getTreeWithAggregate is the same as the result of getTree
     * but just to introduce the two ways
     */

    getTreeWithAggregate(req, res, next) {
        Model.aggregate([
            {
                $lookup: {
                    from: 'books',
                    as: 'books',
                    let: { "categoryId": "$_id" },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $in: ["$$categoryId", "$category"]
                                }
                            }
                        },
                        {
                            $lookup: {
                                from: 'authors',
                                localField: 'author',
                                foreignField: '_id',
                                as: 'author'
                            }
                        },
                        {
                            $lookup: {
                                from: 'categories',
                                localField: 'category',
                                foreignField: '_id',
                                as: 'category'
                            }
                        },
                        {
                            $lookup: {
                                from: 'files',
                                localField: 'image',
                                foreignField: '_id',
                                as: 'image'
                            }
                        },
                        {
                            $addFields: {
                                "image": {
                                    $arrayElemAt: ["$image", 0]
                                }
                            }
                        }
                    ]
                }
            }
        ], (err, response) => {
            if (err) return next(err);
            res.status(200).send({ success: true, response });
        });
    }

    getTree(req, res, next) {
        Model.find({}).populate('books').exec((err, response) => {
            if (err) return next(err);
            res.status(200).send({ success: true, response });
        });
    }

    getAll(req, res, next) {
        Model.find({}, (err, response) => {
            if (err) return next(err);
            res.status(200).send({ success: true, response });
        });
    }

    get(req, res, next) {
        let { id } = req.params;
        Model.findOne({ _id: id }).populate('books').exec((err, response) => {
            if (err) return next(err);
            res.status(200).send({ success: true, response });
        });
    }

    post(req, res, next) {
        let body = req.body;
        let doc = new Model(body);
        doc.save((err, response) => {
            if (err) return next(err);
            res.status(200).send({ success: true, response });
        });
    }

    put(req, res, next) {
        let { id } = req.params;
        let body = req.body;
        Model.updateOne({ _id: id }, {
            $set: body
        }, (err, response) => {
            if (err) return next(err);
            res.status(200).send({ success: true, response });
        });
    }

    delete(req, res, next) {
        let { id } = req.params;
        Model.findByIdAndDelete({ _id: id }, (err, response) => {
            if (err) return next(err);
            res.status(200).send({ success: true, response });
        })
    }

}

const controller = new Controller();
module.exports = controller;